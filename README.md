# JupyterHub on the Kubernetes at MAX IV

This repo contains the deployment configuration files used for running the instance of
JupyterHub on the Kubernetes at [MAX IV](https://www.maxiv.lu.se).

> :warning: The files are provided as it is for reference. They are NOT intended for direct deployment without modification. 
> They do include MAX IV specific hard-codes and specific cases. 

Deployment uses [Zero to JupyterHub with Kubernetes](https://zero-to-jupyterhub.readthedocs.io/en/latest/) Helm Chart 
without modifications, but with highly customized values and relying on the developed extension hooks. 

In particular MAX IV deployment implements:
 * spawning notebooks under authenicated user account UID/GIDs (OIDC and LDAP integration)
 * mounting user and data shares, including profile-specific volumes
 * rich GPU sharing support via [MortalGPU](https://artifacthub.io/packages/helm/mortalgpu/mortalgpu)
 * flexible [Compute Instance profiles](/docs/profileList.md) and RBAC

