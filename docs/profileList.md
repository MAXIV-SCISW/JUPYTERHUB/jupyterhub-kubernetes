# Defining JupyterHub Profiles

Profile list (as defined in `singleuser.profileList`) is a main configuration point for adding the notebooks and compute resources available to end-users.

There are different profiles can be distinguished at MAX IV setup:
  - *Notebook profiles*
  - *Notebook image profiles*
  - *Compute instance profiles*

## Compute Instance Profiles

*Compute Instance* concept at MAX IV setup defines the resource allocation. This includes the requirements to CPU and Memory, placement on the specific node type, GPU allocation.

Compute Instance profile access can be subject to RBAC, so only users holding the defined role can use it.

They are defined as a special `profileList` item, holding the `compute_instances` object.
The keys are unique identifiers of each compute instance profile, with a values defining the resources.

```yaml
  profileList:
    # Compute instance resource profiles
    - compute_instances:
        a100_shared:
          display_name: "Shared A100 (Limits: 100GB RAM, 8 CPU, 8GB GPU RAM)"
          cpu_guarantee: 0.1
          mem_guarantee: 500M
          cpu_limit: 8
          mem_limit: 100G
          gpu_resource_name: jupyterhub/a100-shared
          gpu_guarantee: 4
          gpu_limit: 80
          node_type: a100
        a100_10g:
          display_name: "Dedicated A100 10G Partition (Limits: 50GB RAM, 8 CPU, 10GB GPU RAM)"
          cpu_guarantee: 0.5
          mem_guarantee: 5G
          cpu_limit: 8
          mem_limit: 50G
          gpu_resource_name: jupyterhub/a100-10g
          gpu_profile_usage: true
          gpu_guarantee: 100
          gpu_limit: 100
          node_type: a100
          required_role: a100-10g
          walltime: 3600
```

Available options are:
 - `display_name` - human readable description of the profile shown to user
 - `cpu_guarantee` and `mem_guarantee` - CPU shares and RAM ammount reserved for the Pod. In MAX case guaranteed resources are kept low to allow massive overcommit.
 - `cpu_limit` - max avaiable CPU resource. Throttling will happen if overused. User is seeing this ammount of CPUs available from notebook. Variables like `OMP_NUM_THREADS` are set to this value.
 - `mem_limit` - max allocated memory. Pod is OOM killed if going over limit. User is seeing this ammount of RAM available from notebook.
 - `node_type` - we have 2 type of the nodes now labeled by GPU model inside - `a100` and `v100`. This defined on which nodes to schedule the workload. Pods are balanced between nodes withing the same type
 - `gpu_resource_name` - defined the GPU availability for the Pod. Possible values are `jupyterhub/a100-shared`, `jupyterhub/a100-10g`, `jupyterhub/a100-20g` and `jupyterhub/v100`. If defined the `gpu_guarantee` and `gpu_limit` has to be defined as well.
 - `gpu_profile_usage` - enables the query to Prometheus endpoint to show GPU availability info to the user. Number of schedulable sessions (from GPU availability point of view) will be added to profile description.
 - `gpu_guarantee` - GPU memory in `100MB` units scheduled for Pod. For shared GPU usage guaranteed resource is kept low to allow massive overcommit. Value can be tuned to limit max number of users on the same GPU.
 - `gpu_limit` - max GPU memory in `100MB` units can be used by the Pod. Process will be killed when allocating more GPU memory.
 - `required_role` - if specified, this profile will be available only to the users having the defined role in the `roles` claim of JWT token used to authenticate to Jupyterhub.
 - `walltime` - if specified, the Pod will be terminated after defined perion of time (in seconds)
 - `disabled` - if specified, this profile will be disabled and grayed out in the dropdown. Options value is used as a custom text (e.g. "maintenance") that displayed to end-user next to the `display_name`.

## Notebook Profiles

Other objects in `profileList` list are representing the *Notebook profiles*.

It defines the execution environment selectable by end-user. List of notebook profiles are presented to the user on the main spawn page of the interface. 

Each *Notebook profile* can optionally define:
  - list of applicable *Compute instance profiles* to be selected by end-user
  - list of *Notebook Image Profiles* to choose between different container images

```yaml
  - display_name: "Nordugrid ARCv7 Client"
    slug: arc7
    description: "Development and testing of Nordugrid ARC"
    allowed_compute_instances:
    - cpu_small
    - cpu_large
    kubespawner_override:
      image: harbor.maxiv.lu.se/jupyterhub/arc/base-notebook:u22.04-arc23.05.23-r0
```

Available options are:
 - `display_name` - human readable name of the profile shown to user
 - `slug` - optional id string for profile to refer from custom code
 - `description` - more detailed description of the profile shown to user
 - `kubespawner_override` - object holding the [KubeSpawner](https://jupyterhub-kubespawner.readthedocs.io/en/latest/spawner.html#kubespawner.KubeSpawner) options to define for profile. Typically at least container `image` should be defined.
 - `allowed_compute_instances` - list of unique identifiers of *compute instance profiles* allowed to be used with this *Notebook profile*. This list is a subject to further RBAC-based filtering: if end-user is not allwed to use *compute instance profile* (no role) it will not be offered. If this option is not specified, all compute instance profiles (user have access to) will be offered.
 - `required_role` - if specified, this *Notebook profile* will be available only to the users having the defined role in the `roles` claim of JWT token used to authenticate to Jupyterhub.
 - `profile_options` - defines other selectable *Notebook profile* options, in particular the *Notebook Image Profiles* (see below).

## Notebook Image Profiles

MAX IV custom code distinguish the concept of *Notebook Image Profiles* to choose between different notebook images within a same *Notebook profile*. *Notebook Image Profiles* are optional and can be threted as a second hierarchy level of profiles catalogue.

The `image` object in `profile_options` defines the *Notebook Image Profiles*. Use `display_name` "Image" for convenience and defined the *Notebook Image Profiles* objects under the `choices`.

The keys are unique identifiers of each *Notebook Image Profile*, with a values defining the settings.

```yaml
      profile_options:
        image:
          display_name: Image
          choices:
            conda:
              display_name: "Conda customizable analysis (Load your own conda environments with ipykernels)"
              kubespawner_override:
                image: harbor.maxiv.lu.se/jupyterhub/conda-notebook-ubuntu-bionic:stable
            matlab:
              display_name: "Matlab and Octave analysis (Matlab R2020b, Octave 4.2.2)"
              kubespawner_override:
                image: harbor.maxiv.lu.se/jupyterhub/matlab-notebook-ubuntu-bionic:stable
                extra_volumes:
                  - name: jupyterhub-matlab
                    hostPath:
                      path: "/gpfs/offline1/hpc/sw/pkg/matlab/R2020b"
                      type: Directory
                extra_volume_mounts:
                  - name: jupyterhub-matlab
                    mountPath: /usr/local/MATLAB/R2020b
                    readOnly: true
```

Available options are:
  - `display_name` - human readable description of the profile shown to user
  - `default` - specified once and indicate the default position in the shown drop-down
  - `kubespawner_override` - object holding the values to override the `KubeSpawner` setting of *Notebook Profile*. Note that in the example above the `extra_volumes` and `extra_volume_mounts` are not part of the out-of-box [KubeSpawner options](https://jupyterhub-kubespawner.readthedocs.io/en/latest/spawner.html#kubespawner.KubeSpawner) but implemeneted in the custom MAX IV code.
  - `required_role` - if specified, this *Notebook Image Profile* will be available only to the users having the defined role in the `roles` claim of JWT token used to authenticate to Jupyterhub.


