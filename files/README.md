# Files for Jupyterhub deployment

This directory contains files embeded into Jupyterhub 
deployment via `--set-file` option of `helm`.

They are referred in the `values.yaml` using `extraFiles` 
arrays.

