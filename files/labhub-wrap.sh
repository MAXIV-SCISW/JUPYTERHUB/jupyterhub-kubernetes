#!/bin/bash

#
# MAX IV JHub deployment on Kubernetes spawns notebook with
# user own unpriviledged UID/GID
# Everything in this script runs with the user priviledges
#

# If the notebook (work) directory does not exist, create it
if [ ! -d "$JUPYTER_NOTEBOOKS_DIR" ]; then
    echo "Creating work directory: $JUPYTER_NOTEBOOKS_DIR"
    mkdir -p $JUPYTER_NOTEBOOKS_DIR
    chmod 770 $JUPYTER_NOTEBOOKS_DIR
fi

# Clean up old log files
echo "Clean up old log files"
find $JUPYTER_NOTEBOOKS_DIR -maxdepth 1 -name jupyterlab_*.log -mtime +7 -delete

# Add some symbolic links to data and shared directories inside the
# work directory - useful for the left-hand menu in JupyterLab
echo "Creating symbolic links inside $JUPYTER_NOTEBOOKS_DIR"
mkdir -p $JUPYTER_NOTEBOOKS_DIR/mxn
if [[ $HOME == *"visitors"* ]]; then
    mkdir -p $JUPYTER_NOTEBOOKS_DIR/mxn/visitors
    ln -sf $HOME $JUPYTER_NOTEBOOKS_DIR/mxn/visitors/.
    # Remove directories created by mistake
    rm -rf $JUPYTER_NOTEBOOKS_DIR/mxn/home
else
    mkdir -p $JUPYTER_NOTEBOOKS_DIR/mxn/home
    ln -sf $HOME $JUPYTER_NOTEBOOKS_DIR/mxn/home/.
fi

ln -sf /mxn/groups $JUPYTER_NOTEBOOKS_DIR/mxn/.
ln -sf /data $JUPYTER_NOTEBOOKS_DIR/.

# Fix things for installing conda packages on debian/ubuntu
echo "Setting up local user conda directories"
mkdir -p $HOME/.conda/pkgs/
mkdir -p $HOME/.conda/envs/
touch $HOME/.conda/envs/.conda_envs_dir_test
touch $HOME/.conda/pkgs/urls
chmod 774 $HOME/.conda/pkgs/urls
touch $HOME/.conda/environments.txt

# Fix for user kernels not being found
if [ -d "/usr/local/share/jupyter" ]; then
    mkdir -p $HOME/.local/share/jupyter/kernels
    ln -sf $HOME/.local/share/jupyter/kernels /usr/local/share/jupyter/.
fi

# Links to kernels saved in storage
kernels_path=/sw/jupyterhub/jupyter-kernels/kernel-definitions/share/jupyter/kernels
kernels_link_path=/usr/share/jupyter/kernels
echo "Kernels in $kernels_path"
echo "Symbolic links in $kernels_link_path"

if [ -d "$kernels_link_path" ]; then
    if [ -n "$KERNEL_1" ]; then
        ln -sf $kernels_path/$KERNEL_1 $kernels_link_path/.
        echo "Created symbolic link to $KERNEL_1"
    else
        echo "KERNEL_1 not specified"
    fi
    if [ -n "$KERNEL_2" ]; then
        ln -sf $kernels_path/$KERNEL_2 $kernels_link_path/.
        echo "Created symbolic link to $KERNEL_2"
    else
        echo "KERNEL_2 not specified"
    fi
else
    echo "kernels_link_path: $kernels_link_path does not exist"
fi

# Alter JupyterLab HTML with MAX IV naming
for html in /opt/conda/share/jupyter/lab/static/index.html; do
  [ -f "$html" ] && sed -i.bak 's/JupyterLab/JupyterLab - MAX IV/' $html
done

# workdir is notebook dir
cd $JUPYTER_NOTEBOOKS_DIR

# nvidia-smi wrapper
if [ -n "$NVIDIA_VISIBLE_DEVICES" ]; then
  export PATH="/srv/gpuhelpers:$PATH"
fi

# Set threading settings based on CPU limit
if [ -n "$CPU_LIMIT" ]; then
  OMP_NUM_THREADS=$( printf "%.0f\n" $CPU_LIMIT )
else
  # will work with LXCFS mount as well
  OMP_NUM_THREADS=$( cat /proc/cpuinfo | grep ^processor | wc -l )
fi
export OMP_NUM_THREADS
export MKL_NUM_THREADS=$OMP_NUM_THREADS

# Log file that the user can access
user_log_file=$JUPYTER_NOTEBOOKS_DIR/jupyterlab
[ -n "$JUPYTERHUB_SERVER_NAME" ] && user_log_file="${user_log_file}_${JUPYTERHUB_SERVER_NAME// /_}"
user_log_file=${user_log_file}_$(date +"%Y-%m-%d_%H-%M-%S").log

# Start JupyterLab writting user log
jupyter-labhub $@ 2>&1 | tee $user_log_file
