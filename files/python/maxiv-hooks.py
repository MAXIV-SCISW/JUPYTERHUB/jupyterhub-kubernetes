#
# MAX IV Hooks for Jupyterhub
#
import os
import requests
from tornado import web


def auth_state_hook(spawner, auth_state):
    """hooks to define spawner environment based on user authentication"""
    spawner.log.info("User oauth_state: %s", str(auth_state['oauth_user']))
    # define spawner cmd (mounted from configmap)
    spawner.cmd = ['/srv/maxiv-labhub-start.sh']
    # change the UID/GID
    spawner.uid = auth_state['oauth_user']['uid']
    spawner.gid = auth_state['oauth_user']['gid']
    spawner.supplemental_gids = auth_state['oauth_user']['sgids']
    # change paths
    unix_home_dir = str(auth_state['oauth_user']['unixHomeDirectory'])
    spawner.notebook_dir = '{0}/jupyter_notebooks/'.format(unix_home_dir)
    # get user roles from the token
    roles = auth_state['oauth_user']['roles'] if 'roles' in auth_state['oauth_user'] else []
    # save original profile list (to update if role added dynamically)
    if hasattr(spawner, 'profile_list_config'):
        spawner.profile_list = spawner.profile_list_config[:]
    else:
        spawner.profile_list_config = spawner.profile_list[:]
    # filter profiles
    for p in spawner.profile_list[:]:
        if 'compute_instances' in p:
            continue
        # slug is mandatory
        if 'slug' not in p:
            if 'display_name' in p:
                spawner.log.warning("Filtering out \"%s\" profile: slug should be defined for operation of MAX IV hooks", 
                                    p['display_name'])
            else:
                spawner.log.warning("Filtering out misconfigured profile (no slug and display_name): \"%s\"", str(p))
            spawner.profile_list.remove(p)
            continue
        # by RBAC
        if 'required_role' in p:
            if p['required_role'] not in roles:
                spawner.log.info("Filtering out \"%s\" profile based on RBAC, required_role: %s",
                                 p['display_name'], p['required_role'])
                spawner.profile_list.remove(p)
    # filter profile images by RBAC
    for p in spawner.profile_list:
        try:
            images = p['profile_options']['image']['choices']
            for i in list(images.keys()):
                if 'required_role' in images[i]:
                    if images[i]['required_role'] not in roles:
                        spawner.log.info("Filtering out \"%s\" image in \"%s\" profile based on RBAC, required_role: %s",
                                         i, p['display_name'], images[i]['required_role'])
                        del p['profile_options']['image']['choices'][i]
        except KeyError:
            pass
    # extract MAX IV compute instance profiles from profileList
    spawner.compute_instances = {}
    for p in spawner.profile_list[:]:
        if 'compute_instances' in p:
            for i, v in p['compute_instances'].items():
                # filter compute instance profiles by RBAC
                if 'required_role' in v:
                    if v['required_role'] not in roles:
                        spawner.log.info("Filtering out \"%s\" compute instance profile based on RBAC, required_role: %s",
                                         i, v['required_role'])
                        continue
                # add gpu allocation stats
                if 'gpu_resource_name' in v:
                    if 'gpu_profile_usage' in v and 'gpu_guarantee' in v and v['gpu_profile_usage']:
                        gpu_shares = get_gpus_available_shares(spawner, v['gpu_resource_name'])
                        v['gpu_schedulable'] = int(gpu_shares) // int(v['gpu_guarantee'])
                spawner.compute_instances[i] = v
            spawner.profile_list.remove(p)
    if not spawner.compute_instances:
        spawner.log.error(
            "No compute instance profiles are available to end-user. Raising the error.")
        raise web.HTTPError(
            500, "No Compute Instance profiles are availabe to user. Either the service is missconfigured or user don't have permission to access any of the profiles")
    # add compute instance options to profile list
    for p in spawner.profile_list:
        if 'kubespawner_override' not in p:
            p['kubespawner_override'] = {}
        # add compute profile selector
        allowed_instances = []
        if 'allowed_compute_instances' in p:
            for i in p['allowed_compute_instances']:
                if i in list(spawner.compute_instances.keys()):
                    allowed_instances.append(i)
                else:
                    spawner.log.debug(
                        "Compute instance %s is filtered out from allowed list. Not part of the user's available profiles list.")
            if not allowed_instances:
                p['remove_profile'] = True
                spawner.log.info(
                    "All allowed compute instance profiles are unavailable for user. Profile \"%s\" is marked for removal.", p['display_name'])
                continue
        else:
            # if not specified - all available to user instance profiles are allowed
            allowed_instances = spawner.compute_instances
        # no-choice: single profile allowed
        if len(allowed_instances) == 1:
            p['kubespawner_override'].update(
                spawner.compute_instances[allowed_instances[0]])
        # dropdown with more choises
        else:
            compute_instance_choises = {}
            for n in allowed_instances:
                i = spawner.compute_instances[n]
                # create a display name (with optional gpu allocation)
                display_name = n
                if 'display_name' in i:
                    display_name = i['display_name']
                if 'gpu_schedulable' in i and i['gpu_schedulable'] is not None:
                    display_name += ' [{} available]'.format(i['gpu_schedulable'])
                if 'disabled' in i:
                    display_name = '({}) '.format(i['disabled']) + display_name
                # add compute profile choise
                compute_instance_choises[n] = {
                    "display_name": display_name,
                    "kubespawner_override": i
                }
                if 'disabled' in i:
                    compute_instance_choises[n]['disabled'] = True
            if 'profile_options' not in p:
                p['profile_options'] = {}
            p['profile_options']['compute_profile'] = {
                "display_name": "Compute Profile",
                "choices": compute_instance_choises
            }
    # remove profiles marked for removal
    for p in spawner.profile_list[:]:
        if 'remove_profile' in p:
            spawner.profile_list.remove(p)
    spawner.log.debug("Profile list: {}", str(spawner.profile_list))
    # fail if everything is filtered out
    if not spawner.profile_list:
        raise web.HTTPError(
            500, "No notebook profiles are availabe to user. Either the service is missconfigured or user don't have permission to access any of the profiles or resrources.")

    # add username and email as extra pod annotations
    if 'name' in auth_state['oauth_user']:
        spawner.extra_annotations["jupyterhub.maxiv.lu.se/username"] = auth_state['oauth_user']['name']
    if 'email' in auth_state['oauth_user']:
        spawner.extra_annotations["jupyterhub.maxiv.lu.se/email"] = auth_state['oauth_user']['email']

    # add extra pod specs
    spawner.extra_pod_config = {
        "hostPID": False,
        "topologySpreadConstraints": [{
            "maxSkew": 2,
            "topologyKey": 'kubernetes.io/hostname',
            "whenUnsatisfiable": 'ScheduleAnyway',
            "labelSelector": {
                "matchLabels": {
                    "heritage": 'jupyterhub'
                }
            }
        }]
    }
    # add extra container config
    spawner.extra_container_config = {
        "imagePullPolicy": "Always"
    }
    # setup Environment Variables
    nb_user = str(auth_state['oauth_user']['preferred_username'])
    spawner.environment['NB_USER'] = nb_user
    spawner.environment['HOME'] = unix_home_dir
    spawner.environment['PWD'] = unix_home_dir
    spawner.environment['JUPYTER_NOTEBOOKS_DIR'] = '{0}/jupyter_notebooks/'.format(
        unix_home_dir)


def arc_profile_spawn(spawner):
    """pecial hook for Nordugrid ARC profile"""
    spawner.log.info(
        "Going to start extra container with Token Helper service")
    # api token to access user_auth_state
    token_helper = TokenHelper(spawner.user)
    token_helper.clenup_tokens()
    # run token-helper to securely get access_token in a separate container
    spawner.extra_containers.append({
        "name": "token-helper",
        "image": "harbor.maxiv.lu.se/jupyterhub/arc/token-helper:v0.1",
        "env": [
            {"name": "JUPYTERHUB_API_TOKEN", "value": token_helper.new_token()},
            {"name": "JUPYTERHUB_API_URL", "value": "http://hub:8081/hub/api"},
            {"name": "JUPYTERHUB_USER", "value": "{username}"}
        ],
        "securityContext": {
            "runAsUser": spawner.uid,
            "runAsGroup": spawner.gid
        }
    })


def pre_spawn_hook(spawner):
    """hook for spawner adjustments (incl. profile-based)"""
    spawner.log.info("Preparing to spawn the user notebook based on the choise: %s", str(
        spawner.user_options))
    nb_profile = spawner.user_options.get('profile')
    nb_image = spawner.user_options.get('image')
    nb_compute = spawner.user_options.get('compute_profile')
    # enforcement of RBAC (against form hijecking)
    if nb_compute not in spawner.compute_instances:
        spawner.log.error(
            "Compute instance %s is not availabe for user in pre-spawn. Raising the error.", nb_compute)
        raise web.HTTPError(
            500, "Compute Instance profile \"{}\" is not availabe to user or user have no permissions to use it.".format(nb_compute))
    # Get profile's kubespanwer_override
    spawner_override = {}
    for p in spawner.profile_list:
        if 'slug' in p and p['slug'] == nb_profile:
            # generic profile kubespanwer_override
            if 'kubespawner_override' in p:
                spawner_override = p['kubespawner_override']
            # per-image kubespanwer_override defined in profile_options
            if nb_image is not None:
                try:
                    image_spawner_override = p['profile_options']['image']['choices'][nb_image]['kubespawner_override']
                except KeyError:
                    image_spawner_override = {}
                spawner_override.update(image_spawner_override)
            # compute instance kubespanwer_override defined in profile_options
            if nb_compute is not None:
                try:
                    image_spawner_override = p['profile_options']['compute_profile'][
                        'choices'][nb_compute]['kubespawner_override']
                except KeyError:
                    image_spawner_override = {}
                spawner_override.update(image_spawner_override)
            break
    spawner.log.info('Using kubespawner_override: %s', str(spawner_override))
    # GPU requests via MortalGPU Device Plugin
    if 'gpu_resource_name' in spawner_override:
        gpu_resource_name = spawner_override['gpu_resource_name']
        if 'gpu_guarantee' in spawner_override:
            # kubernetes does not support overcommit of kustom resources, so limits always equal to requests
            spawner.extra_resource_guarantees = {
                gpu_resource_name: spawner_override['gpu_guarantee']}
            spawner.extra_resource_limits = {
                gpu_resource_name: spawner_override['gpu_guarantee']}
            # overcommit in MortalGPU is implemented via annotation
            # if no limit is specified it will be limited to requested guarantee!
            if 'gpu_limit' in spawner_override:
                spawner.extra_annotations['gpu-mem-limit.{}'.format(
                    gpu_resource_name)] = str(spawner_override['gpu_limit'])
    # Affinity (especially for GPU nodes to be honored in topologySphreadConstrains)
    if 'node_type' in spawner_override:
        spawner.node_affinity_required = [{
            "matchExpressions": [{
                "key": "maxiv.lu.se/node-type",
                "operator": "In",
                "values": [spawner_override['node_type']]
            }]
        }]
    # Extra volume mounts
    extra_volumes = [v['name'] for v in spawner.volumes]
    extra_mounts = [m['mountPath'] for m in spawner.volume_mounts]
    if 'extra_volumes' in spawner_override:
        for ev in spawner_override['extra_volumes']:
            if ev['name'] not in extra_volumes:
                spawner.volumes.append(ev)
    if 'extra_volume_mounts' in spawner_override:
        for vm in spawner_override['extra_volume_mounts']:
            if vm['mountPath'] not in extra_mounts:
                spawner.volume_mounts.append(vm)
    # LXCFS extra mounts
    # /proc/xxx mounts
    for pf in ['cpuinfo', 'diskstats', 'loadavg', 'meminfo', 'stat', 'swaps', 'uptime']:
        vname = 'lxcfs-proc-' + pf
        if vname not in extra_volumes:
            spawner.volumes.append({
                'name': vname,
                'hostPath': {'path': '/var/lib/lxcfs/proc/' + pf}
            })
            spawner.volume_mounts.append({
                'name': vname,
                'mountPath': '/proc/' + pf
            })
    # /sys/devices/system/cpu/online
    if 'lxcfs-sys-cpu-online' not in extra_volumes:
        spawner.volumes.append({
            'name': 'lxcfs-sys-cpu-online',
            'hostPath': {'path': '/var/lib/lxcfs/sys/devices/system/cpu/online'}
        })
        spawner.volume_mounts.append({
            'name': 'lxcfs-sys-cpu-online',
            'mountPath': '/sys/devices/system/cpu/online',
        })
    # Enforce notebook walltime if defined in profile
    if 'walltime' in spawner.compute_instances[nb_compute]:
        # mount emptyDir for communication between walltime-helper and notebook
        if 'waltime-status' not in extra_volumes:
            spawner.volumes.append({
                'name': 'waltime-status',
                'emptyDir': {
                    'medium': 'Memory',
                    'sizeLimit': '1Mi'
                }
            })
            spawner.volume_mounts.append({
                'name': 'waltime-status',
                'mountPath': '/var/run/walltime'
            })
        # run countdown termination script in the adjacent walltime-helper container
        walltime_script = """
            echo $WALLTIME > /var/run/walltime/defined
            while [ $WALLTIME -gt 0 ]; do
                echo $WALLTIME > /var/run/walltime/timeleft
                sleep 1
                WALLTIME=$(( WALLTIME - 1))
            done
            # loop in case jupyterhub is unavailable at the time of request
            while true; do
                if [ -z "$$JUPYTERHUB_SERVER_NAME" ]; then
                    curl -X DELETE -H \"Authorization: token $JUPYTERHUB_API_TOKEN\" $JUPYTERHUB_API_URL/users/$JUPYTERHUB_USER/server
                else
                    curl -X DELETE -H \"Authorization: token $JUPYTERHUB_API_TOKEN\" $JUPYTERHUB_API_URL/users/$JUPYTERHUB_USER/server/$JUPYTERHUB_SERVER_NAME
                fi
                sleep 30
            done
        """
        spawner.extra_containers.append({
            "name": "walltime-terminator",
            "image": "quay.io/curl/curl:8.2.1",
            "command": [ "/bin/sh", "-c", walltime_script ],
            "env": [
                {"name": "WALLTIME", "value": str(spawner.compute_instances[nb_compute]['walltime'])},
                {"name": "JUPYTERHUB_API_TOKEN", "value": spawner.api_token },
                {"name": "JUPYTERHUB_API_URL", "value": "http://hub:8081/hub/api"},
                {"name": "JUPYTERHUB_USER", "value": "{username}"},
                {"name": "JUPYTERHUB_SERVER_NAME", "value": "{servername}"}
            ],
            "volumeMounts": [
                {"name": "waltime-status", "mountPath": "/var/run/walltime"}
            ],
            "securityContext": {
                "runAsUser": spawner.uid,
                "runAsGroup": spawner.gid
            }
        })
    # special config for Nordugrid ARC client
    if nb_profile == 'arc7':
        arc_profile_spawn(spawner)


def option_form_hook(spawner):
    """Dummy version of the options form renderer to make it work after auth hook"""
    return spawner._options_form_default()

def options_from_form_hook(formdata):
     """Interpret form data"""
     profile = formdata.get('profile', [None])[0]
     image_key = 'profile-option-{}--image'.format(profile)
     compute_profile_key = 'profile-option-{}--compute_profile'.format(profile)
     options = {
        'profile': profile,
        'image': formdata.get(image_key, [None])[0],
        'compute_profile': formdata.get(compute_profile_key, [None])[0],
     }
     return options

def get_gpus_available_shares(spawner, resource_name):
    """Query prometheus endpoint for available MortalGPU shares"""
    prometheus = os.getenv('PROMETHEUS_ENDPOINT')
    query = 'scalar(sum(metagpu_device_shares{{resource_name="{resource_name}"}}) - sum(metagpu_device_allocated_shares{{resource_name="{resource_name}"}}))'.format(resource_name = resource_name)
    if prometheus is None:
        spawner.log.error(
            "Cannot query available shares for %s. Prometheus URL is not defined in the PROMETHEUS_ENDPOINT env variable.", resource_name)
        return None
    try:
        response = requests.get(prometheus + '/api/v1/query', params={'query': query})
        if response.status_code != 200:
            spawner.log.error(
                "Cannot query available shares for %s. Prometheus query returns %s", resource_name, response.status_code)
            return None
        result = response.json()
        if 'status' not in result or result['status'] != 'success':
            spawner.log.error(
                "Cannot query available shares for %s. Prometheus query was not successfull: %s", resource_name, str(result))
            return None
        return str(result['data']['result'][1])
    except requests.exceptions.RequestException as e:
        spawner.log.error(
            "Cannot query available shares for %s. Error: %s", resource_name, str(e))
        return None
