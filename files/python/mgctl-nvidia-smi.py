#!/usr/bin/env python

import json
import subprocess

def get_human_readable_size(sizeinbytes):
    """generate human-readable size representation"""
    output_fmt = '{0}'
    for unit in ['B', 'KiB', 'MiB']:
        if sizeinbytes < 1024.0:
            return output_fmt.format(sizeinbytes, unit)
        output_fmt = '{0:.0f}{1}'
        sizeinbytes /= 1024.0
    return '{0:.1f}GiB'.format(sizeinbytes)

# print output from nvidia-smi tool as it is up to 'Processes'
nvidiasmi = "/usr/bin/nvidia-smi"
cmd = subprocess.Popen(nvidiasmi, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, shell=True)

prevline = ''
delimiter = ''
for line in cmd.stdout:
    line = line.decode('utf-8')
    print(line, end = '')
    if 'Processes' in line:
        delimiter = prevline
    if delimiter and '==========' in line:
        break
    prevline = line

# load container processes data from mgctl
mgctl = "/usr/bin/mgctl get processes -o json"
cmd = subprocess.Popen(mgctl, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, shell=True)
for line in cmd.stdout:
    data = json.loads(line)
    break

# different versions of nvidia-smi have different output width, construct process description string based on nvidia-smi delimiter width
w = len(delimiter)
cmdline_w = w - 38 - 15
process_format = '| {device:>4} {gpu_instance:>5} {compute_instance:>4} {pid:>9} {type:>6}   {cmdline:>' + str(cmdline_w) + '}  {mem_usage:>10} |\n'

processes_output = ''
for container in data:
    if 'device_processes' in container:
        for p in container['device_processes']:
            cmdline = p['cmdline']
            cmdline = '...' + cmdline
            if len(cmdline) > cmdline_w:
                cmdline = cmdline[-(cmdline_w+1):]
                cmdline = '...' + cmdline[3:]
            gi = 'N/A'
            ci = 'N/A'
            if 'gpu_instance_id' in p:
                gi = p['gpu_instance_id']
                if gi < 0 or gi > 100:
                    gi = 'N/A'
            if 'compute_instance_id' in p:
                ci = p['compute_instance_id']
                if ci < 0 or ci > 100:
                    ci = 'N/A'
            process_data = {
                'device': 0,  # TODO: not exposed in mgctl
                'gpu_instance': gi,
                'compute_instance': ci,
                'pid': p['pid'],
                'type': 'C',  # TODO: not exposed in mgctl
                'cmdline': cmdline,
                'mem_usage': '{}'.format(get_human_readable_size(p['memory']))
            }
            processes_output += process_format.format(**process_data)

processes_output += delimiter
print(processes_output)
