# class to manage tokens to access user auth_state data
from sqlalchemy import inspect
from tornado.log import app_log

class TokenHelper(object):
    _token_note = ''
    _user = None
    _db = None

    def __init__(self, user):
        self._user = user
        self._username = user.name
        self._db = inspect(user).session
        self._token_note = 'Access token helper for {}'.format(self._username)

    def new_token(self):
        app_log.info('Creating token for access_token helper for user %s', self._username)
        token = self._user.new_api_token(note=self._token_note, roles=['user_auth_state'])
        return token

    def clenup_tokens(self):
        for token in self._user.api_tokens:
            if token.note == self._token_note:
                app_log.info('Cleaning access_token helper token %s for user %s', token.id, self._username)
                self._db.delete(token)
                self._db.commit()

