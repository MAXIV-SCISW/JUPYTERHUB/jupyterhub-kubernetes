# MAX IV Jupyterhub deployment on Kubernetes

This directory contains the values file for Jupyterhub deployment that is used
by CI/CD pipeline to provision review and and than production instance on
Kubernetes.

Secret part of values is stored in the Gitlab CI/CD variables at MAX IV gitlab:

```
config:
  ldap:
    bind:
      user: 'my secret bind user'
      secret: 'my secret bind password'
```

