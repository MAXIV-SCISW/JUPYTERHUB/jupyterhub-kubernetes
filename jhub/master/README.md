# Using set-file option in Helm

The `values.yaml` assumes the deployment will be performed with file content substitution with `--set-file` option.

Here is the full list of substitutions needed:
```
--set-file hub.extraFiles.logo.binaryData=./files/logo/maxiv-jupyterkub.b64
--set-file hub.extraFiles.tpl_login.stringData=./files/templates/login.html
--set-file hub.extraFiles.tpl_spawn.stringData=./files/templates/spawn.html
--set-file hub.extraFiles.tpl_error.stringData=./files/templates/error.html
--set-file hub.extraFiles.tpl_form.stringData=./files/templates/form.html
--set-file hub.extraFiles.token_helper.stringData=./files/python/token-helper.py
--set-file hub.extraFiles.hooks.stringData=./files/python/maxiv-hooks.py
--set-file singleuser.extraFiles.labhub_start.stringData=./files/labhub-wrap.sh
--set-file singleuser.extraFiles.mgctl_nvidia_smi.stringData=./files/python/mgctl-nvidia-smi.py
```

