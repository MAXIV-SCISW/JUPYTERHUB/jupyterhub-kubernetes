# LDAP to Configmap

This helper deployment performing syncing name servicess data (``/etc/passwd`` and ``/etc/group``) from AD LDAP and store them in the Kubernetes ConfigMap.

ConfigMap than mounted inside the Jupyter Notebook container by JupyterHub deployment ensuring UID/GID awareness.

The directory contains:
 * ``container`` - service code and Docker image build files
 * ``deployment`` - Helm answers file for CI/CD service deployment from Gitlab
 * ``ldap2cmap`` - Helm chart files
 * ``static`` - Legacy manifests for deploying manually without Helm (only for the reference)

