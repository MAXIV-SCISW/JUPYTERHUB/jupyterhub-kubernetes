import sys
import ssl
import os
import logging

from ldap3 import Server, Connection, Tls, ALL, ALL_ATTRIBUTES
import ldap3.core.exceptions

from kubernetes import client, config
from kubernetes.client.rest import ApiException
from io import StringIO

# set glogal loglevel
logging.getLogger().setLevel(os.getenv("LOGLEVEL", default="DEBUG").strip())
# create logger for ldad2cmap
logger = logging.getLogger('ldap2cmap')
handler = logging.StreamHandler()
handler.setFormatter(logging.Formatter(
    '%(asctime)s - %(name)s - %(levelname)s - %(message)s'))
logger.addHandler(handler)

#
# Setup Kubernetes API connection
#

config.load_incluster_config()
api_instance = client.CoreV1Api()

# read namespace
try:
    with open("/var/run/secrets/kubernetes.io/serviceaccount/namespace") as nsf:
        k8s_namespace = nsf.read()
except EnvironmentError:
    logger.critical("Cannot get current namespace. Exiting.")
    sys.exit(1)

# configmap names
configmap_passwd = os.getenv('CONFIGMAP_PASSWD', default="etc-passwd").strip()
configmap_group = os.getenv('CONFIGMAP_GROUP', default="etc-group").strip()

# define configmap objects
cmaps = {}
for c_type, c_name in [("passwd", configmap_passwd), ("group", configmap_group)]:
    cmap = client.V1ConfigMap()
    cmap.metadata = client.V1ObjectMeta(name=c_name)
    cmap.data = {}
    cmap.data[c_type] = ""
    cmaps[c_type] = cmap

    # read existing configmaps or create and new empty one
    try:
        inkube_cmap = api_instance.read_namespaced_config_map(
            name=c_name, namespace=k8s_namespace)
        logger.info('In-cluster configmap %s data includes %s records',
                    c_name, len(inkube_cmap.data[c_type].splitlines()))
    except ApiException as e:
        logger.info("ConfigMap %s does not exist yet. Creating.", c_name)
        try:
            api_instance.create_namespaced_config_map(
                namespace=k8s_namespace, body=cmaps[c_type])
        except ApiException as e:
            logger.critical('Failed to create configmap. Error: %s', str(e))
            sys.exit(1)
    else:
        cmaps[c_type].data[c_type] = inkube_cmap.data[c_type]
#
# Generate new data querying LDAP
#
# Microsoft Active Directory set an hard limit of 1000 entries returned by any search.
# So it is better to always set up a paged search when dealing with AD.


def get_groups(conn):
    data = {}
    entry_generator = conn.extend.standard.paged_search(
        search_base=ldap_base,
        search_filter='(&(objectClass=group)(gidNumber=*))',
        attributes=[
            'sAMAccountName',
            'gidNumber',
            'memberOf'
        ],
        paged_size=1000,
        generator=True
    )
    for e in entry_generator:
        if 'dn' in e:
            data[e['dn']] = e['attributes']
            data[e['dn']]['members'] = set()
    return data


def add_group_user(memberof, user, groups, nlevel=1):
    for g in memberof:
        if g in groups:
            groups[g]['members'].add(user)
            if groups[g]['memberOf'] and nlevel < 5:
                add_group_user(groups[g]['memberOf'], user, groups, nlevel+1)


def get_users(conn, groups):
    data = {}
    entry_generator = conn.extend.standard.paged_search(
        search_base=ldap_base,
        search_filter='(&(objectClass=person)(uidNumber=*))',
        attributes=[
            'sAMAccountName',
            'uidNumber',
            'gidNumber',
            'displayName',
            'unixHomeDirectory',
            'loginShell',
            'memberOf'
        ],
        paged_size=1000,
        generator=True
    )
    for e in entry_generator:
        if 'dn' in e:
            data[e['dn']] = e['attributes']
            if data[e['dn']]['memberOf']:
                add_group_user(data[e['dn']]['memberOf'],
                               data[e['dn']]['sAMAccountName'], groups)
    return data


def render_passwd(users):
    pf = StringIO()
    pf.write(ubuntu_passwd_header)
    for udn in sorted(users.keys()):
        u = users[udn]
        name = u['displayName'] if u['displayName'] else u['sAMAccountName']
        shell = u['loginShell'] if u['loginShell'] else '/bin/bash'
        home = u['unixHomeDirectory'] if u['unixHomeDirectory'] else '/home/' + \
            u['sAMAccountName']
        pf.write('{}:*:{}:{}:{}:{}:{}\n'.format(
            u['sAMAccountName'],
            u['uidNumber'],
            u['gidNumber'],
            name, home, shell
        ))
    return pf


def render_group(groups):
    gf = StringIO()
    gf.write(ubuntu_group_header)
    for gdn in sorted(groups.keys()):
        g = groups[gdn]
        gf.write('{}:*:{}:{}\n'.format(
            g['sAMAccountName'],
            g['gidNumber'],
            ','.join(sorted(list(g['members'])))
        ))
    return gf


ubuntu_passwd_header = """root:x:0:0:root:/root:/bin/bash
daemon:x:1:1:daemon:/usr/sbin:/usr/sbin/nologin
bin:x:2:2:bin:/bin:/usr/sbin/nologin
sys:x:3:3:sys:/dev:/usr/sbin/nologin
sync:x:4:65534:sync:/bin:/bin/sync
games:x:5:60:games:/usr/games:/usr/sbin/nologin
man:x:6:12:man:/var/cache/man:/usr/sbin/nologin
lp:x:7:7:lp:/var/spool/lpd:/usr/sbin/nologin
mail:x:8:8:mail:/var/mail:/usr/sbin/nologin
news:x:9:9:news:/var/spool/news:/usr/sbin/nologin
uucp:x:10:10:uucp:/var/spool/uucp:/usr/sbin/nologin
proxy:x:13:13:proxy:/bin:/usr/sbin/nologin
www-data:x:33:33:www-data:/var/www:/usr/sbin/nologin
backup:x:34:34:backup:/var/backups:/usr/sbin/nologin
list:x:38:38:Mailing List Manager:/var/list:/usr/sbin/nologin
irc:x:39:39:ircd:/var/run/ircd:/usr/sbin/nologin
gnats:x:41:41:Gnats Bug-Reporting System (admin):/var/lib/gnats:/usr/sbin/nologin
nobody:x:65534:65534:nobody:/nonexistent:/usr/sbin/nologin
_apt:x:100:65534::/nonexistent:/usr/sbin/nologin
"""

ubuntu_group_header = """root:x:0:
daemon:x:1:
bin:x:2:
sys:x:3:
adm:x:4:
tty:x:5:
disk:x:6:
lp:x:7:
mail:x:8:
news:x:9:
uucp:x:10:
man:x:12:
proxy:x:13:
kmem:x:15:
dialout:x:20:
fax:x:21:
voice:x:22:
cdrom:x:24:
floppy:x:25:
tape:x:26:
sudo:x:27:
audio:x:29:
dip:x:30:
www-data:x:33:
backup:x:34:
operator:x:37:
list:x:38:
irc:x:39:
src:x:40:
gnats:x:41:
shadow:x:42:
utmp:x:43:
video:x:44:
sasl:x:45:
plugdev:x:46:
staff:x:50:
games:x:60:
users:x:100:
nogroup:x:65534:
mlocate:x:101:
ssh:x:102:
"""

ldap_server = os.getenv("LDAP_SERVER", default="adauth.maxiv.lu.se").strip()
ldap_user = os.getenv("LDAP_USER").strip()
ldap_secret = os.getenv("LDAP_SECRET").strip()
ldap_base = os.getenv("LDAP_BASEDN", default="DC=maxlab,DC=lu,DC=se").strip()

if ldap_server is None:
    logger.critical("LDAP_SERVER should be defined.")
    sys.exit(1)

if ldap_user is None:
    logger.critical("LDAP_USER should be defined.")
    sys.exit(1)

if ldap_secret is None:
    logger.critical("LDAP_SECRET should be defined.")
    sys.exit(1)

try:
    tls = Tls(validate=ssl.CERT_NONE, version=ssl.PROTOCOL_TLS_CLIENT,
              ciphers='DEFAULT:@SECLEVEL=1')
    server = Server(ldap_server, use_ssl=True, tls=tls, get_info=ALL)
    conn = Connection(server, user=ldap_user,
                      password=ldap_secret, auto_bind=True)
except ldap3.core.exceptions.LDAPException as err:
    logger.critical(
        'Failed to establish connection to LDAP server. Error: %s', str(err))
    sys.exit(1)

# get AD LDAP data
groups = get_groups(conn)
users = get_users(conn, groups)

# render passwd/group
render_data = {
    "passwd": render_passwd(users).getvalue(),
    "group": render_group(groups).getvalue()
}

exit_code = 0
for c_type, c_name in [("passwd", configmap_passwd), ("group", configmap_group)]:
    if not render_data[c_type]:
        logger.error(
            "There is no data obtained from LDAP for %s. Skipping config update.", c_type)
        exit_code = 1
        continue
    if cmaps[c_type].data[c_type] == render_data[c_type]:
        logger.info(
            "LDAP data is still the same for %s, will not update %s ConfigMap", c_type, c_name)
        continue
    # update configmap data
    logger.info(
        "LDAP data for %s had changed. Updating %s ConfigMap data.", c_type, c_name)
    cmaps[c_type].data[c_type] = render_data[c_type]

    try:
        api_instance.replace_namespaced_config_map(name=c_name, namespace=k8s_namespace,
                                                   body=cmaps[c_type])
    except ApiException as e:
        logger.error("Failed to update ConfigMap %s. Error: %s",
                     c_name, str(e))
        exit_code = 1

sys.exit(exit_code)
