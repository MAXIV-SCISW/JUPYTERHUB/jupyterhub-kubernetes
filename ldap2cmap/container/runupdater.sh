#!/bin/sh
SCRIPT_RUNDIR="$( cd "$( dirname "$0" )" >/dev/null 2>&1 && pwd )"

while true; do
   python ${SCRIPT_RUNDIR}/ldap2configmap.py || exit 1
   sleep ${SYNC_INTERVAL:-3600}
done
