{{/* vim: set filetype=mustache: */}}
{{/*
Expand the name of the chart.
*/}}
{{- define "ldap2cmap.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 48 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 48 chars because some Kubernetes name fields are limited to this (by the DNS naming spec)
and we append suffixes to the template.
If release name contains chart name it will be used as a full name.
*/}}
{{- define "ldap2cmap.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 48 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name }}
{{- .Release.Name | trunc 48 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" .Release.Name $name | trunc 48 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "ldap2cmap.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 48 | trimSuffix "-" }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "ldap2cmap.labels" -}}
helm.sh/chart: {{ include "ldap2cmap.chart" . }}
{{ include "ldap2cmap.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "ldap2cmap.selectorLabels" -}}
app.kubernetes.io/name: {{ include "ldap2cmap.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{/*
Create the name of the service account to use
*/}}
{{- define "ldap2cmap.serviceAccountName" -}}
{{- if .Values.serviceAccount.create }}
{{- default (include "ldap2cmap.fullname" .) .Values.serviceAccount.name }}
{{- else }}
{{- default "default" .Values.serviceAccount.name }}
{{- end }}
{{- end }}
