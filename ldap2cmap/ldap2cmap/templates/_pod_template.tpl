{{/*
Pod template (metadata)
*/}}
{{- define "ldap2cmap.pod_metadata_template" -}}
{{- with .Values.podAnnotations -}}
annotations:
  {{- toYaml . | nindent 2 }}
{{- end -}}
labels:
  {{- include "ldap2cmap.selectorLabels" . | nindent 2 }}
{{- end }}

{{/*
Pod template (general spec)
*/}}
{{- define "ldap2cmap.pod_spec_template" -}}
{{- with .Values.imagePullSecrets -}}
imagePullSecrets:
  {{- toYaml . | nindent 2 }}
{{- end -}}
serviceAccountName: {{ include "ldap2cmap.serviceAccountName" . }}
securityContext:
  {{- toYaml .Values.podSecurityContext | nindent 2 }}
containers:
  - name: {{ .Chart.Name }}
    {{- if .Deployment }}
    command: ["/app/runupdater.sh"]
    {{- end }}
    securityContext:
      {{- toYaml .Values.securityContext | nindent 6 }}
    image: "{{ .Values.image.repository }}:{{ .Values.image.tag | default .Chart.AppVersion }}"
    imagePullPolicy: {{ .Values.image.pullPolicy }}
    resources:
      {{- toYaml .Values.resources | nindent 6 }}
    env:
      - name: CONFIGMAP_PASSWD
        value: {{ .Values.config.cmap_passwd | quote }}
      - name: CONFIGMAP_GROUP
        value: {{ .Values.config.cmap_group | quote }}
      {{- if .Deployment }}
      - name: SYNC_INTERVAL
        value: {{ .Values.pod_controller.deployment.sync | quote }}
      {{- end }}
      - name: LOGLEVEL
        value: {{ .Values.config.loglevel | quote }}
      - name: LDAP_SERVER
        value: {{ required "LDAP server should be defined" .Values.config.ldap.server | quote }}
      - name: LDAP_BASEDN
        value: {{ required "LDAP base DN should be defined" .Values.config.ldap.basedn | quote }}
      - name: LDAP_USER
        valueFrom:
          secretKeyRef:
            name: {{ include "ldap2cmap.fullname" . }}-ldap-bind 
            key: binduser
      - name: LDAP_SECRET
        valueFrom:
          secretKeyRef:
            name: {{ include "ldap2cmap.fullname" . }}-ldap-bind
            key: password
{{- with .Values.nodeSelector }}
nodeSelector:
  {{- toYaml . | nindent 2 }}
{{- end }}
{{- with .Values.affinity }}
affinity:
  {{- toYaml . | nindent 2 }}
{{- end }}
{{- with .Values.tolerations }}
tolerations:
  {{- toYaml . | nindent 2 }}
{{- end }}
{{- end }}

{{/*
Pod template (wrapper for deployment spec)
*/}}
{{- define "ldap2cmap.pod_spec_template_deployment" -}}
{{- include "ldap2cmap.pod_spec_template" (dict "Values" .Values "Chart" .Chart "Release" .Release "Deployment" true) }}
{{- end }}