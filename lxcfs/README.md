# LXCFS DaemonSet

[LXCFS](https://linuxcontainers.org/lxcfs/introduction/) is designed to overcome current limitations of the Linux kernel in respect to provide CGroup-aware values for resource usage inside the continer.

LXCFS is FUSE filesystem that should be bind-mounted over container /proc

For Jupyterhub use-case it allows Notebook user to see the Memory/CPU limit and container uptime instead of values for the entire host. This makes the user experiense more seamless.

## DeamonSet

The LXCFS on Kubernetes cluster is deployed as ``DaemonSet`` that runs priviledged container with ``hostPID`` enabled and CGroups tree mounted inside. It should be deplyed by Kubernetes cluster admin under the system namespace.

Compared to installation on the host itself it just proides a transpatent delivery of the feature from Kubernetes side, eliminating the need to maintaining it separately.

The repo contains the container image Dockerfile (based on [this work](https://github.com/denverdino/lxcfs-admission-webhook)) and Helm Chart for DaemonSet deployment atomation.

## LXCFS mounts

Bind-mounts are handled by JupyterHub KubeSpwner invocation code as we anyway have full control on the spawned Notebooks pod specification. No addmission controller involved in this case to avoid overcomplication.

